package ru.starikov.hw1.task3;

public class Game {
    private static final int MIN_HEAP_SIZE = 1;

    private Reader reader;
    private Computer computer;

    Game(Reader reader, Computer computer) {
        this.reader = reader;
        this.computer = computer;
    }

    public void start(GameConditions gameConditions) {

        while (gameConditions.getHeapSize() > MIN_HEAP_SIZE) {
            Integer heapSize = gameConditions.getHeapSize();
            Integer handCapacity = gameConditions.getHandCapacity();
            Integer numberTakenObjects;
            boolean isHumanTurn = gameConditions.getHumanTurn();

            if (isHumanTurn) {
                numberTakenObjects = reader.readNumberTakenObjects(handCapacity);
            } else {
                numberTakenObjects = computer.getNumberTakenObjects(heapSize, gameConditions.getHandCapacity());
            }
            gameConditions.setHeapSize(heapSize - numberTakenObjects);
            gameConditions.setHumanTurn(!isHumanTurn);
            System.out.println("Размер кучи " + gameConditions.getHeapSize());
        }

        if (!gameConditions.getHumanTurn()) {
            System.out.println("You win");
        } else {
            System.out.println("You lost");
        }

    }
}
