package ru.starikov.hw1.task3;

public class GameConditions {
    private Integer heapSize;
    private Integer handCapacity;
    private boolean isHumanTurn;

    public Integer getHeapSize() {
        return heapSize;
    }

    public Integer getHandCapacity() {
        return handCapacity;
    }

    public boolean getHumanTurn() {
        return isHumanTurn;
    }

    public void setHeapSize(Integer heapSize) {
        this.heapSize = heapSize;
    }

    public void setHandCapacity(Integer handCapacity) {
        this.handCapacity = handCapacity;
    }

    public void setHumanTurn(boolean humanTurn) {
        isHumanTurn = humanTurn;
    }

    @Override
    public String toString() {
        return "Размер кучи " + heapSize + ", размер руки " + handCapacity;
    }
}
