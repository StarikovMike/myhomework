package ru.starikov.hw1.task1;

public class DateUtils {

    private static boolean isYearLeap(Integer year) {
        return (year % 4 == 0);
    }

    public static int getMaxMonthDay(Integer month, Integer year) {
        return getMaxMonthDays(year)[month];
    }

    private static int[] getMaxMonthDays(int year) {
        int[] monthArray = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (isYearLeap(year)) {
            monthArray[1] = 29;
        }
        return monthArray;
    }
}
