package ru.starikov.hw1.task2;

public class Runner {
    private static final double speed = 10;
    private static final double acceleration = 1.1;

    public static void main(String[] args) {
        System.out.println("\nВ 10-ый день спортсмен пробежит " + getDistanceByDay(10) + " км");
        System.out.println("\nВпервые спортсмен пробежит больше 20 км на " + getDayByDistance(20) + " день");
        System.out.println("\nСуммарный пробег спортсмена превысит 100 км на " + getDayByTotalDistance(100) + " день");
    }

    private static double getDistanceByDay(int day) {
        double result = speed;
        for (int i = 0; i < day; i++) {
            result *= acceleration;
        }
        return result;
    }

    private static int getDayByTotalDistance(double distance) {
        double traveledDistance = speed;
        int day = 0;
        while (distance > 0) {
            distance -= traveledDistance;
            day++;
            traveledDistance *= acceleration;
        }
        return day;
    }

    private static int getDayByDistance(double distance) {
        double traveledDistance = speed;
        int day = 1;
        while (traveledDistance < distance) {
            traveledDistance *= acceleration;
            day++;
        }
        return day;
    }
}
