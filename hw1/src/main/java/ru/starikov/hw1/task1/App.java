package ru.starikov.hw1.task1;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Validator validator = new Validator();

        Reader reader = new Reader(scanner, validator);
        Date date = reader.readDate();

        Calculator calculator = new Calculator();
        Date nextDate = calculator.getNextDay(date);
        System.out.println(nextDate);
    }
}
