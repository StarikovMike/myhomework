package ru.starikov.hw1.task1;

import static ru.starikov.hw1.task1.DateUtils.getMaxMonthDay;

public class Calculator {

    private static final Integer MAX_MONTH_NUMBER = 12;
    private static final Integer MIN_MONTH_NUMBER = 1;
    private static final Integer MIN_DAY_NUMBER = 1;

    public Date getNextDay(Date date) {
        Integer day = date.getDay();
        Integer month = date.getMonth();
        Integer year = date.getYear();

        Date nextDay = new Date(date);
        nextDay.setDay(++day);

        if (day > getMaxMonthDay(month, year)) {
            nextDay.setDay(MIN_DAY_NUMBER);
            nextDay.setMonth(++month);
        }

        if (month > MAX_MONTH_NUMBER) {
            nextDay.setMonth(MIN_MONTH_NUMBER);
            nextDay.setYear(++year);
        }

        return nextDay;
    }

}
