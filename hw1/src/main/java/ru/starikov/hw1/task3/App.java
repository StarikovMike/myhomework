package ru.starikov.hw1.task3;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Validator validator = new Validator();

        Reader reader = new Reader(scanner, validator);
        GameConditions gameConditions = reader.readGameConditions();

        Computer computer = new Computer();
        Game game = new Game(reader, computer);
        game.start(gameConditions);
    }

}
