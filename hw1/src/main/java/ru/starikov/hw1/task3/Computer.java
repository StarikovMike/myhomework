package ru.starikov.hw1.task3;

public class Computer {

    private static final int MIN_HEAP_SIZE = 1;
    private static final int MIN_NUMBER_TAKEN_OBJECTS = 1;

    public Integer getNumberTakenObjects(Integer heapSize, Integer handCapacity) {

        int n = (heapSize - MIN_HEAP_SIZE - (heapSize - MIN_HEAP_SIZE) % (handCapacity + 1)) / handCapacity;
        if (heapSize <= handCapacity + MIN_HEAP_SIZE) {
            return heapSize - MIN_HEAP_SIZE;
        } else if ((handCapacity + 1) * n + MIN_HEAP_SIZE == heapSize) {
            return MIN_NUMBER_TAKEN_OBJECTS;
        } else if ((handCapacity + 1) * n + MIN_HEAP_SIZE + (heapSize - MIN_HEAP_SIZE) % (handCapacity + 1) == heapSize) {
            return (heapSize - MIN_HEAP_SIZE) % (handCapacity + 1);
        } else {
            return MIN_NUMBER_TAKEN_OBJECTS;
        }
    }
}
