package ru.starikov.hw1.task1;

import static ru.starikov.hw1.task1.DateUtils.getMaxMonthDay;

public class Validator {

    private static final int MAX_MONTH_NUMBER = 12;
    private static final int MIN_YEAR_NUMBER = 1;
    private static final int MIN_MONTH_NUMBER = 1;
    private static final int MIN_DAY_NUMBER = 1;


    public void validate(Date date) throws Exception{
        Integer day = date.getDay();
        Integer month = date.getMonth();
        Integer year = date.getYear();

        if (year < MIN_YEAR_NUMBER) {
            throw new Exception();
        }

        if (month < MIN_MONTH_NUMBER || month > MAX_MONTH_NUMBER) {
            throw new Exception();
        }

        if (day < MIN_DAY_NUMBER || day > getMaxMonthDay(month, year)) {
            throw new Exception();
        }
    }
}
