package ru.starikov.hw1.task3;

import java.util.Scanner;

public class Reader {

    private final Scanner scanner;
    private final Validator validator;

    public Reader(Scanner scanner, Validator validator) {
        this.scanner = scanner;
        this.validator = validator;
    }

    public GameConditions readGameConditions() {
        GameConditions gameConditions = new GameConditions();
        do {
            try {
                System.out.println("Введите размер кучи");
                gameConditions.setHeapSize(Integer.valueOf(scanner.nextInt()));
                System.out.println("Введите размер руки");
                gameConditions.setHandCapacity(Integer.valueOf(scanner.nextInt()));
                System.out.println("Введите true, чтобы ходить первым и false, чтобы первым ходил компьютер");
                gameConditions.setHumanTurn(Boolean.valueOf(scanner.nextBoolean()));
                validator.validateGameConditions(gameConditions.getHeapSize(), gameConditions.getHandCapacity());
                return gameConditions;
            } catch (Exception e) {
                System.out.println("Введены неккоректные данные");
            }
        } while (true);
    }

    public Integer readNumberTakenObjects(Integer handCapacity) {
        Integer numberTakenObjects;
        do {
            try {
                System.out.println("Введите количество забираемых предметов");
                numberTakenObjects = scanner.nextInt();
                validator.validateNumberTakenObjects(numberTakenObjects, handCapacity);
                return numberTakenObjects;
            } catch (Exception e) {
                System.out.println("Введены неккоректные данные");
            }

        } while (true);
    }

}
