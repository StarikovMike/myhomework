package ru.starikov.hw1.task1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Reader {

    private final Scanner scanner;
    private final Validator validator;

    public Reader(Scanner scanner, Validator validator) {
        this.scanner = scanner;
        this.validator = validator;
    }

    public Date readDate() {
        Date date = new Date();

            do {
                try {
                    System.out.println("Введите день");
                    date.setDay(Integer.valueOf(scanner.nextInt()));
                    System.out.println("Введите месяц");
                    date.setMonth(Integer.valueOf(scanner.nextInt()));
                    System.out.println("Введите год");
                    date.setYear(Integer.valueOf(scanner.nextInt()));
                    validator.validate(date);
                    return date;
                } catch (Exception e) {
                    System.out.println("Введены неккоректные данные");
                }
            } while (true);
    }
}
