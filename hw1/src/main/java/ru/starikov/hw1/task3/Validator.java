package ru.starikov.hw1.task3;

public class Validator {

    private static final int MIN_HEAP_SIZE = 1;
    private static final int MIN_HAND_CAPACITY_SIZE = 1;
    private static final int MIN_NUMBER_TAKEN_OBJECTS = 1;

    public void validateGameConditions(Integer heapSize, Integer handCapacity) throws Exception{

        if (heapSize < MIN_HEAP_SIZE) {
            throw new Exception();
        }

        if (handCapacity < MIN_HAND_CAPACITY_SIZE) {
            throw new Exception();
        }

        if (handCapacity >= heapSize) {
            throw new Exception();
        }
    }

    public void validateNumberTakenObjects(Integer numberTakenObjects, Integer handCapacity) throws Exception{

        if (numberTakenObjects > handCapacity) {
            throw new Exception();
        }

        if (numberTakenObjects < MIN_NUMBER_TAKEN_OBJECTS) {
            throw new Exception();
        }
    }
}
